FROM python:3.8-buster
WORKDIR /whatbot
COPY README.md setup.py ./
COPY whatbot ./whatbot
RUN pip install -U .
ENV TOKEN export-your-valid-discord-bot-token
ENTRYPOINT whatbot $OPTIONS run -t $TOKEN --database /whatbot/data/whatbot.sqlite
