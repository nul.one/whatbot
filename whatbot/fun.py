"""
Fun and such.
"""
from discord.ext import commands
from whatbot import Cogs
from whatbot import WhatException
from whatbot import utils
import discord
import random

neat_decode = {
        "neat": 0,
        "neaT": 1,
        "neAt": 2,
        "neAT": 3,
        "nEat": 4,
        "nEaT": 5,
        "nEAt": 6,
        "nEAT": 7,
        "Neat": 8,
        "NeaT": 9,
        "NeAt": 10,
        "NeAT": 11,
        "NEat": 12,
        "NEaT": 13,
        "NEAt": 14,
        "NEAT": 15,
        }

neat_code = {
        0: "neat",
        1: "neaT",
        2: "neAt",
        3: "neAT",
        4: "nEat",
        5: "nEaT",
        6: "nEAt",
        7: "nEAT",
        8: "Neat",
        9: "NeaT",
        10: "NeAt",
        11: "NeAT",
        12: "NEat",
        13: "NEaT",
        14: "NEAt",
        15: "NEAT",
        }


class Fun(commands.Cog, name="fun"):
    """Randoms."""

    def neat_to_human(self, text):
        text = text.strip()
        if len(text)%8 != 0:
            raise WhatException("Not a neat code.")
        human_bytes = bytearray()
        for i in range(0, int(len(text)/8)):
            first = text[i*8:i*8+4]
            second = text[i*8+4:i*8+8]
            print(first)
            print(second)
            b = neat_decode[first]*16 + neat_decode[second]
            print(b)
            human_bytes.append(b)
        return human_bytes.decode('utf-8')

    def human_to_neat(self, text):
        neat_b = text.encode('utf-8')
        neat_text = ""
        for b in neat_b:
            i = int(b)
            first = int(i/16)
            second = i-(16*first)
            neat_text += neat_code[first]
            neat_text += neat_code[second]
        return neat_text

    @commands.command()
    async def neat(self, ctx, *, text: str):
        """Neat translator."""
        output = ""
        try:
            output = self.neat_to_human(text)
        except:
            output = self.human_to_neat(text)
        return await ctx.send(output)
 
Cogs().add_cog(Fun)

