'''
?watbot base classes.
'''
from datetime import datetime
from enum import Enum
from pony import orm
from whatbot import Config
from whatbot import utils
from whatbot.database import db, get_or_create
import json
import random

class User(db.Entity):
    id = orm.PrimaryKey(int, size=64, auto=False)
    memberships = orm.Set("Member", reverse='user')

class Guild(db.Entity):
    id = orm.PrimaryKey(int, size=64, auto=False)
    prefix = orm.Required(str, default=Config().default_prefix)
    members = orm.Set("Member", reverse='guild')
    actions = orm.Set("Action", reverse='guild')
    _settings = orm.Required(str, default='{}')

    @property
    def settings(self):
        d = json.loads(self._settings)
        d.setdefault("fame_gain_rate", "1")
        d.setdefault("fame_max_gain", "25")
        d.setdefault("fame_min_gain", "15")
        d.setdefault("fame_time_segment", "60")
        d.setdefault("keep_roles", "0")
        d.setdefault("joiner_role", None)
        return d

    @settings.setter
    def settings(self, settings):
        float(settings['fame_gain_rate'])
        float(settings['fame_time_segment'])
        int(settings['fame_max_gain'])
        int(settings['fame_min_gain'])
        int(settings['keep_roles'])
        self._settings = json.dumps(settings)

class Action(db.Entity):
    guild = orm.Required(Guild, reverse='actions')
    name = orm.Required(str)
    orm.composite_key(guild, name)
    _items = orm.Required(str, default='[]')

    @property
    def items(self):
        items_tuple = tuple(json.loads(self._items))
        return items_tuple

    @items.setter
    def items(self, items_tuple):
        self._items = json.dumps(items_tuple)

class Member(db.Entity):
    user = orm.Required(User, reverse='memberships')
    guild = orm.Required(Guild, reverse='members')
    orm.composite_key(user, guild)
    fame = orm.Required(int, default=0)
    last_fame_gain_time = orm.Required(float, default=0.0)
    _roles = orm.Required(str, default='[]')

    @property
    def roles(self):
        roles_tuple = tuple(json.loads(self._roles))
        return roles_tuple

    @roles.setter
    def roles(self, roles_tuple):
        self._roles = json.dumps(roles_tuple)

    def chat_fame_gain(self, message):
        timestamp = datetime.timestamp(message.created_at)
        time_passed = timestamp - self.last_fame_gain_time
        if time_passed > int(self.guild.settings["fame_time_segment"]):
            base_fame_bonus = random.randint(
                    int(self.guild.settings["fame_min_gain"]),
                    int(self.guild.settings["fame_max_gain"])
                    )
            fame_bonus = int(base_fame_bonus * float(self.guild.settings["fame_gain_rate"]))
            self.last_fame_gain_time = timestamp
            self.fame += fame_bonus

@orm.db_session
def update_member_roles(discord_member):
    member = get_or_create(Member,
            user=get_or_create(User, id=discord_member.id),
            guild=get_or_create(Guild, id=discord_member.guild.id)
            )
    member.roles = tuple(map(lambda r: r.mention, discord_member.roles[1:]))

@orm.db_session
def roles_to_restore(discord_member):
    guild=get_or_create(Guild, id=discord_member.guild.id)
    if not int(guild.settings['keep_roles']):
        return
    user=get_or_create(User, id=discord_member.id)
    member = get_or_create(Member, user=user, guild=guild)
    roles_to_restore = []
    for role in member.roles:
        for guild_role in discord_member.guild.roles:
            if role == guild_role.mention:
                roles_to_restore.append(guild_role)
    return roles_to_restore

@orm.db_session
def joiner_role(discord_member):
    guild = get_guild(discord_member.guild.id)
    joiner_role = guild.settings['joiner_role']
    if joiner_role:
        for role in discord_member.guild.roles:
            if role.mention == joiner_role:
                return role

@orm.db_session
def prefix(bot, message):
    if not message.guild:
        return Config().default_prefix
    guild = get_or_create(Guild, id=message.guild.id)
    return guild.prefix

@orm.db_session
def activity(message):
    if not message.guild:
        return
    member = get_or_create(Member,
            user=get_or_create(User, id=message.author.id),
            guild=get_or_create(Guild, id=message.guild.id)
            )
    member.chat_fame_gain(message)

@orm.db_session
def get_guild_action(guild_id, name):
    guild = Guild.get(id=guild_id)
    action = Action.get(guild=guild, name=name)
    return action

@orm.db_session
def get_guild(guild_id):
    guild = Guild.get(id=guild_id)
    return guild

@orm.db_session
def get_member_by_ids(user_id, guild_id):
    user = User.get(id=user_id)
    guild = Guild.get(id=guild_id)
    if not user or not guild:
        return
    member = Member.get(
            user=user,
            guild=guild
            )
    return member

