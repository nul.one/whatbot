"""
General commands.
"""
from discord.ext import commands
from whatbot import base
from whatbot import utils
from whatbot.bot import bot
import discord
import whatbot

@bot.command()
async def version(ctx):
    """Display ?whatbot version."""
    await ctx.send(whatbot.__version__)

@bot.command()
async def embed(ctx, title: str=None, *, message: str=None):
    '''Embed a message.
    Usage: embed "Some title" Some message.
    Bot will send your embeded message in same channel and delete the original.
    '''
    if not title or not message:
        error = utils.make_error(description="Missing arguments. Usage: embed \"Some Title\" Some message.")
        return await ctx.send(embed=embed)
    embed = utils.make_embed(author=ctx.message.author)
    embed.title = title
    embed.description = message
    await ctx.send(embed=embed)
    await ctx.message.delete()

@bot.command()
async def avatar(ctx, user: discord.User = None):
    '''Display user avatar.
    Usage: avatar [USER_TAG|USER_ID]
    '''
    if user is None:
        user = ctx.author
    return await ctx.send(user.avatar_url)

@bot.command()
async def info(ctx, d_user: discord.User = None, full=None):
    '''User info.
    Usage: info [(USER_TAG|USER_ID) [full]]
    Show user information. If you want more verbose output, add 'full' at the end.
    '''
    if d_user is None:
        d_user = ctx.author
    d_member = None
    try:
        d_member = ctx.guild.get_member(d_user.id)
    except:
        pass
    date_format = "%Y-%m-%d %H:%M"
    embed = utils.make_embed()
    embed.title = str(d_user)
    embed.set_thumbnail(url=d_user.avatar_url)
    if d_member:
        embed.description = d_member.mention
        member = base.get_member_by_ids(user_id=d_member.id, guild_id=ctx.guild.id)
        embed.add_field(name="🎖️ Fame", value=member.fame if member else 0)
        embed.add_field(name="📅 Joined", value=d_member.joined_at.strftime(date_format))
        if len(d_member.roles) > 1:
            role_string = ' '.join([r.mention for r in d_member.roles][1:])
            embed.add_field(name=f"🏷️ Roles [{len(d_member.roles)-1}]", value=role_string, inline=False)
    if full and "full".startswith(full.lower()):
        if d_member:
            members = sorted(ctx.guild.members, key=lambda m: m.joined_at)
            if not d_member.bot:
                embed.add_field(name="#️⃣ Join position", value=str(  list(filter(lambda x: not x.bot, members)).index(d_member)  +1))
        embed.add_field(name="📄 Registered", value=d_user.created_at.strftime(date_format))
        if d_member:
            perm_string = ', '.join([str(p[0]).replace("_", " ").title() for p in d_member.guild_permissions if p[1]])
            embed.add_field(name="Guild permissions", value=perm_string or "None", inline=False)
    embed.set_footer(text='ID: ' + str(d_user.id))
    return await ctx.send(embed=embed)

