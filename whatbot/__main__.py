"""
?what bot main module.
"""
from whatbot import Config
from whatbot import bot
import click
import discord
import os
import whatbot as app

@click.group()
@click.version_option(version=app.__version__,
    message=f"%(prog)s %(version)s - {app.__copyright__}")
@click.option('-d', '--debug', is_flag=True,
    help="Enable debug mode with output of each action in the log.")
@click.pass_context
def cli(ctx, **kwargs): # pragma: no cover
    #conf = WhatConfig()
    if ctx.params.get('debug'):
        print("Starting in debug mode. Errors shown in Discord will contain stack trace.")
        Config().debug = True

@cli.command()
@click.option('-t', '--token', required=True, default="",
    help="Authorization token.")
@click.option('--database', required=False, default=None,
    help="Path to sqlite database file.")
def run(**kwargs): # pragma: no cover
    "Run bot service."
    if not kwargs['token']:
        raise WhatException("No token provided.")
    if kwargs['database']:
        db_path = os.path.abspath(os.path.expanduser(kwargs['database']))
        Config().db_path = db_path
    bot.run(kwargs['token'])

if __name__ == '__main__': # pragma: no cover
    cli()

