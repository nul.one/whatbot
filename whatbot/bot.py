"""
?what bot.
"""
from discord.ext import commands
from whatbot import Cogs
from whatbot import Config
from whatbot import base
from whatbot import utils
from whatbot.database import db, bind_database
import discord
import importlib
import random
import traceback
import whatbot

discord_intents = discord.Intents.default()
discord_intents.members = True
help_command = commands.DefaultHelpCommand(
        no_category = 'general'
        )
bot = commands.Bot(
        command_prefix=base.prefix,
        description="?what bot",
        intents=discord_intents,
        help_command = help_command,
        )

def run(token):
    bind_database(Config().db_path)
    #orm.set_sql_debug(True)
    db.generate_mapping(create_tables=True)
    bot.run(token)

@bot.event
async def on_message(message):
    if message.author.bot:
        return
    if message.content == "?":
        await message.channel.send("?what")
    base.activity(message)
    await bot.process_commands(message)

@bot.event
async def on_member_join(discord_member):
    roles_to_restore = base.roles_to_restore(discord_member)
    if roles_to_restore:
        existing_roles_to_restore = list(filter(
            lambda r: r in discord_member.guild.roles, roles_to_restore
            ))
        await discord_member.add_roles(
                *existing_roles_to_restore,
                reason="Restoring roles that user had before leaving guild."
                )
    joiner_role = base.joiner_role(discord_member)
    if joiner_role:
        await discord_member.add_roles(
                joiner_role,
                reason="Applying joiner role."
                )

@bot.event
async def on_member_update(before, after):
    base.update_member_roles(after)

@bot.event
async def on_guild_join(discord_guild):
    for discord_member in discord_guild.members:
        if not discord_member.bot:
            base.update_member_roles(discord_member)

@bot.event
async def on_command_error(ctx, error):
    lm = utils.LongMessage(ctx, 'error')
    if Config().debug:
        for step in traceback.format_exception(
                etype=type(error),
                value=error,
                tb=error.__traceback__
                ):
            lm.add_line(step)
        lm.reply(ctx.message)
    else:
        lm.add_line(str(error))
    await lm.reply(ctx.message)
    raise error

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} with id {bot.user.id}')
    print('------')

from whatbot import actions
from whatbot import emoji
from whatbot import fun
from whatbot import general
from whatbot import leaderboard
from whatbot import mod
from whatbot import options
from whatbot import rng
Cogs().register_bot(bot)

