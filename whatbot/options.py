"""
Moderator commands.
"""
from discord.ext import commands
from pony import orm
from whatbot import Cogs
from whatbot import base
from whatbot import utils

class Options(commands.Cog, name="options"):
    """View or update options and settings."""

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def settings(self, ctx, variable: str=None, value: str=None):
        '''Display and update server settings.
        Usage:
            settings  - display all settings variables.
            settings VARIABLE  - display specific variable only.
            settings VARIABLE VALUE  - set new value for variable.
        '''
        embed = utils.make_embed(msg_type="info")
        embed.title = "Guild Settings"
        if variable is None:
            settings = self._get_guild_settings(ctx.guild.id)
            settings_list = ""
            for key in sorted(settings):
                settings_list += f"{key}: {settings[key]}\n"
            embed.add_field(name="Variables", value=f"{settings_list}")
            return await ctx.send(embed=embed)
        if value is None:
            settings = self._get_guild_settings(ctx.guild.id)
            embed.add_field(name=variable, value=f"{settings.get(variable, 'N/A')}")
            return await ctx.send(embed=embed)
        settings = self._get_guild_settings(ctx.guild.id)
        if variable not in settings:
            embed = utils.make_error(f"**{variable}** is an unknown variable.")
            return await ctx.send(embed=embed)
        settings[variable] = value
        self._save_guild_settings(ctx.guild.id, settings)
        embed = utils.make_embed(msg_type="done")
        embed.title = "Guild Settings"
        embed.description = f"Variable **{variable}** updated!"
        return await ctx.send(embed=embed)

    @orm.db_session
    def _get_guild_settings(self, guild_id):
        return base.Guild[guild_id].settings

    @orm.db_session
    def _save_guild_settings(self, guild_id, settings):
        guild = base.Guild[guild_id]
        guild.settings = settings

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def prefix(self, ctx, prefix: str):
        '''Set new prefix.'''
        self._set_new_guild_prefix(ctx.guild.id, prefix)
        embed = utils.make_embed(msg_type="done")
        embed.title = "Prefix"
        embed.add_field(name="New prefix set:", value=prefix)
        return await ctx.send(embed=embed)

    @orm.db_session
    def _set_new_guild_prefix(self, guild_id, prefix: str):
        guild = base.Guild[guild_id]
        guild.prefix = prefix

Cogs().add_cog(Options)

