"""
Emoji related commands.
"""
from discord.ext import commands
from whatbot import Cogs
import discord

class Emoji(commands.Cog, name="emoji"):
    """Emoji related commands."""

    @commands.command()
    async def geturl(self, ctx, emoji: discord.Emoji):
        '''Get url to emoji image.'''
        await ctx.send(emoji.url)

Cogs().add_cog(Emoji)

