"""
Leaderboard commands.
"""
from discord.ext import commands
from pony import orm
from whatbot import Cogs
from whatbot import base
from whatbot import utils
import discord

class Leaderboard(commands.Cog, name="leaderboard"):
    """Leaderboard."""

    rank_emoji = {
            1: "🥇",
            2: "🥈",
            3: "🥉",
            4: "4",
            5: "5",
            6: "6",
            7: "7",
            8: "8",
            9: "9",
            10: "10",
            11: "11",
            12: "12",
            13: "13",
            14: "14",
            15: "15",
            16: "16",
            17: "17",
            18: "18",
            19: "19",
            20: "20",
            }

    @commands.command(aliases=["lb"])
    @commands.guild_only()
    async def leaderboard(self, ctx):
        """Display leaderboard."""
        top20 = self._top20(ctx.guild.id)
        leaderboard = ""
        i = 0
        for member in top20:
            i+=1
            d_user = await ctx.bot.fetch_user(member.user.id)
            if not d_user:
                continue
            leaderboard += f"{self.rank_emoji[i]} {d_user.mention} 🎖️{member.fame}\n"
        embed = utils.make_embed()
        embed.set_author(
                name=ctx.guild.name,
                icon_url=ctx.guild.icon_url
                )
        embed.title = "🎖️ Leaderboard"
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.description = leaderboard
        return await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def rank(self, ctx, d_user: discord.User = None):
        """Display member rank."""
        if d_user is None:
            d_user = ctx.author
        member, rank = self._member_and_rank(ctx.guild.id, d_user.id)
        embed = utils.make_embed()
        embed.set_author(
                name=ctx.guild.name,
                icon_url=ctx.guild.icon_url
                )
        embed.description = d_user.mention
        embed.set_thumbnail(url=d_user.avatar_url)
        if member:
            rank_text = f"{self.rank_emoji[rank]} place with {member.fame} fame."
            embed.add_field(name="Rank", value=rank_text)
        else:
            embed.add_field(name="Rank", value="N/A")
        return await ctx.send(embed=embed)

    @orm.db_session
    def _top20(self, guild_id):
        top20 = base.Member.select(
                lambda m: m.guild.id == guild_id).order_by(orm.desc(base.Member.fame))[:20]
        return top20

    @orm.db_session
    def _member_and_rank(self, guild_id, user_id):
        member = base.get_member_by_ids(user_id, guild_id)
        if member is None:
            return None, 0
        members = base.Member.select(
                lambda m: m.guild.id == guild_id).order_by(orm.desc(base.Member.fame))[:]
        rank = members.index(member) + 1
        return member, rank

Cogs().add_cog(Leaderboard)

