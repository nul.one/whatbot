"""
?what - a Discord bot.
"""
__version__ = "0.1.3"
__license__ = "BSD"
__year__ = "2021"
__author__ = "Predrag Mandic"
__author_email__ = "predrag@nul.one"
__copyright__ = f"Copyright {__year__} {__author__} <{__author_email__}>"

class WhatException(Exception):
    "Generic ?what exception."
    pass

def Singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance

@Singleton
class Cogs():
    cogs = []
    
    def add_cog(self, cog):
        self.cogs.append(cog)

    def register_bot(self, bot):
        for cog in self.cogs:
            bot.add_cog(cog(bot))

@Singleton
class Config():
    db_path = ":memory:"
    debug = False
    default_prefix = "?"

