"""
Custom user (re)actions.
"""
from discord.ext import commands
from pony import orm
from whatbot import Cogs
from whatbot import WhatException
from whatbot import base
from whatbot import utils
from whatbot.database import get_or_create
import discord
import random

class Actions(commands.Cog, name="actions"):
    """Custom user (re)actions."""

    @commands.command(name="action", aliases=["a"])
    @commands.guild_only()
    async def action(self, ctx, name: str, *, target: str=""):
        """Perform an action on target user."""
        name, index = self.name_index_from_string(name)
        action = self.get_action(guild_id=ctx.guild.id, name=name)
        if not action:
            raise WhatException(f"Action {name} does not exist.")
        try:
            target_user = ctx.bot.get_user(int(target.strip("<@!>")))
        except:
            target_user = None
        image_url = None
        if index and index > len(action.items):
            raise WhatException(f"Max index for action `{action.name}` is `{len(action.items)}`.")
        index = index or random.randint(1, len(action.items))
        image_url = action.items[index-1]
        embed = utils.make_embed('fun', author = ctx.message.author)
        if target_user:
            embed.set_thumbnail(url=target_user.avatar_url)
        embed.set_image(url=image_url)
        embed.description = f"{ctx.message.author.mention} {action.name}s {target_user.mention if target_user else target}"
        embed.set_footer(text=f"{action.name}#{index}")
        await ctx.send(embed=embed)

    @commands.command(name="action-add", aliases=["a-a"])
    @commands.guild_only()
    async def action_add(self, ctx, name: str, url: str):
        """Add new url to existing or new action.
        Example:
            `action-add burn https://i.gifer.com/XiPp.gif`
            This will add an url to a "burn" action. If the action does not exist, it will be created.
        """
        if not name.isalpha():
            raise WhatException("Action name can contain only letters.")
        name = name.lower()
        action = self.get_or_create_action(ctx.guild.id, name)
        items = list(action.items)
        if url not in items:
            items.append(url)
            self.update_action(action, tuple(items))
        embed = utils.make_embed('done')
        embed.set_thumbnail(url=url)
        embed.description = f"Url present in `{action.name}` at index `{items.index(url)+1}`."
        await ctx.send(embed=embed)

    @commands.command(name="action-remove", aliases=["a-r"])
    @commands.guild_only()
    async def action_remove(self, ctx, name: str):
        """Remove whole action or an url from existing action.
        Examples:
            `action-remove hug` removes action hug
            `action-remove hug#15` removes url at index 15 from action hug
        """
        name, index = self.name_index_from_string(name)
        action = self.get_action(ctx.guild.id, name)
        embed = utils.make_embed('done')
        if action is None:
            raise WhatException(f"Action `{name}` does not exist.")
        if index is None:
            self.delete_action(action)
            embed.description = f"Action '{action.name}' is now deleted."
            await ctx.send(embed=embed)
            return
        items = list(action.items)
        if len(items) < index:
            raise WhatException(f"Max index for action `{action.name}` is `{len(items)}`.")
        if len(items) == 1 and index == 1:
            self.delete_action(action)
            embed.description = f"Action '{action.name}' is now deleted."
            await ctx.send(embed=embed)
            return
        del(items[index-1])
        self.update_action(action, tuple(items))
        embed.description = f"Url on index {index} is deleted from action '{action.name}'."
        await ctx.send(embed=embed)

    @commands.command(name="action-list", aliases=["a-l"])
    @commands.guild_only()
    async def action_list(self, ctx, name: str=None):
        """List available actions and number of url's."""
        actions = self.list_actions(ctx.guild.id)
        lm = utils.LongMessage(ctx)
        if not actions:
            lm.title = "Actions"
            lm.add_line("N/A")
            await lm.reply(ctx.message)
            return
        if not name:
            lm.title = "Actions"
            lm.content.separator = " "
            for action in actions:
                lm.content.add_line(f"{action.name}[{len(action.items)}]")
            await lm.reply(ctx.message)
            return
        action = self.get_action(ctx.guild.id, name)
        lm.title = action.name
        i=0
        for url in action.items:
            i+=1
            lm.add_line(f"**#{i}**\n{url}\n")
        await lm.reply(ctx.message)

    def name_index_from_string(self, s: str):
        name = ""
        index = None
        if "#" in s:
            try:
                name, index = s.split("#")
                index = int(index)
                name = name.lower()
                assert index > 0
                assert name.isalpha()
            except:
                raise WhatException("Action name and index number need to be in format `action#index`. Index must exist")
        else:
            try:
                assert s.isalpha()
                name = s.lower()
            except:
                raise WhatException("Action name can contain only letters.")
        return name, index

    @orm.db_session
    def update_action(self, action, items):
        old_action = base.Action[action.id]
        old_action.items = items

    @orm.db_session
    def delete_action(self, action):
        old_action = base.Action[action.id]
        old_action.delete()

    @orm.db_session
    def get_action(self, guild_id, name):
        guild=base.Guild.get(id=guild_id)
        action=base.Action.get(guild=guild, name=name)
        return action

    @orm.db_session
    def get_or_create_action(self, guild_id, name):
        guild=base.Guild.get(id=guild_id)
        action=get_or_create(base.Action, guild=guild, name=name)
        return action

    @orm.db_session
    def list_actions(self, guild_id):
        guild=base.Guild.get(id=guild_id)
        actions = base.Action.select(lambda a: a.guild == guild).order_by(base.Action.name)[:]
        return actions

Cogs().add_cog(Actions)

