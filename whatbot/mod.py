"""
Moderator commands.
"""
from discord.ext import commands
from whatbot import Cogs
from whatbot import utils
import discord

class Mod(commands.Cog, name="mod"):
    """Moderator commands."""

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, amount="1"):
        """Clear previous messages in channel.
        Usage: clear N
        """
        try:
            amount = int(amount)
        except:
            embed = utils.make_error("First argument needs to be number of messages to clear.")
            return await ctx.send(embed=embed)
        if amount < 1:
            embed = utils.make_error("I need a positive number of messages to clear.")
            return await ctx.send(embed=embed)
        if amount > 100:
            embed = utils.make_error("I refuse to clear more than 100 messages at once.")
            return await ctx.send(embed=embed)
        await ctx.message.delete()
        result = await ctx.channel.purge(limit=(amount))
        embed = utils.make_embed(msg_type="done")
        embed.title = "Clear"
        embed.description = f"Cleared {len(result)} messages in {ctx.channel.mention}."
        await utils.short_message(ctx, embed)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, d_member: discord.Member, *, reason: str=None):
        """Kick a member from guild.
        Usage: kick USER_TAG|USER_ID
        """
        if d_member.guild_permissions.administrator:
            embed = utils.make_error("Can't kick an admin.")
            await ctx.send(embed=embed)
            return
        try:
            embed = utils.make_embed()
            embed.title = "Kick"
            embed.description = f"You are about to be kicked from **{ctx.guild.name}**."
            embed.add_field(name="Reason", value=reason)
            await d_member.send(embed=embed)
        except Exception as e:
            embed = utils.make_error(description=f"Could not DM {d_member.mention} about reasons for getting kicked.")
            embed.add_field(name="Info", value=e)
            await ctx.send(embed=embed)
        await ctx.guild.kick(d_member, reason=reason)
        embed = utils.make_embed('done')
        embed.title = 'Kick'
        embed.description = f"{d_member.mention} was kicked from the server!"
        embed.add_field(name="Reason", value=reason)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, d_user: discord.User, *, reason: str=None):
        """Ban a member from guild.
        Usage: ban USER_TAG|USER_ID
        """
        d_member = ctx.guild.get_member(d_user.id)
        if d_member:
            if d_member.guild_permissions.administrator:
                embed = utils.make_error("Can't ban an admin.")
                await ctx.send(embed=embed)
                return
            try:
                embed = utils.make_embed()
                embed.title = "Ban"
                embed.description = f"You are about to be banned from **{ctx.guild.name}**."
                embed.add_field(name="Reason", value=reason)
                await d_member.send(embed=embed)
            except Exception as e:
                embed = utils.make_error(description=f"Could not DM {d_member.mention} about reasons for getting banned.")
                embed.add_field(name="Info", value=e)
                await ctx.send(embed=embed)
        await ctx.guild.ban(d_user, reason=reason)
        embed = utils.make_embed('done')
        embed.title = 'Ban'
        embed.description = f"{d_user.mention} was banned from the server!"
        embed.add_field(name="Reason", value=reason)
        await ctx.send(embed=embed)
        
    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, d_user: discord.User):
        """Unban a user.
        Usage: unban USER_TAG|USER_ID
        """
        await ctx.guild.unban(d_user)
        embed = utils.make_embed('done')
        embed.title = "Ban lift"
        embed.description = f"Ban for {d_user.mention} has been lifted."
        await ctx.send(embed=embed)

Cogs().add_cog(Mod)

