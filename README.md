
?what bot
==================================================

A Discord bot.

Requirements
-------------------------

- docker
- docker-compose

How to run
-------------------------

- Clone the repo
- Export `TOKEN` variable containing your discord bot token.
- Run the container: `docker-compose up`

